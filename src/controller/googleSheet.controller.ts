import { google } from 'googleapis';
import { Request, Response } from "express";
import {GetAllData, FindDataByID, CreateNewSheet, CreateNewData, AutoMapingToGoogleSheet, CreateSchemaForSheet, GetSchema, AutoMapingToObject, UpdateData} from '../service/googlesheet.service'
import { Product } from '../models/product.model';
import { EnumTextValue, HttpStatusCode } from '../utils/enum';

/** Create Flexible Data : Done test */
export async function CreateFlexibleData(req: Request, res: Response){
    const body = req.body;
    const schema = await GetSchema(body.range);
    const dataAfterMapping = AutoMapingToGoogleSheet(body.data, schema);
    const dataCreate = await CreateNewData(dataAfterMapping, body.range);
    if(dataCreate == false || dataCreate == undefined){
        return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
            status: HttpStatusCode.INTERNAL_SERVER_ERROR,
            statusText: EnumTextValue.CREATE_FAIL,
            data: EnumTextValue.STRING_EMPTY
        })
    }else{
        return res.status(dataCreate.status).json({
            status: dataCreate.status,
            statusText: dataCreate.statusText,
            data: dataCreate.data
        })
    }
    
}

/** Create Flexible Sheet : Done test */
export async function CreateFlexibleSheet(req: Request, res: Response){
    const body = req.body;
    const countElement = Object.keys(body.data).length;
    /** option when create sheet */
    //const arrData = CreateSchemaForSheet(body.data, countElement);
    /** get object type to create sheet */
    const arrKeys = Object.keys(body.data);
    const dataCreateNewSheet = await CreateNewSheet(body.range, arrKeys);
    return res.status(dataCreateNewSheet.status).json({
        status: dataCreateNewSheet.status,
        statusText: dataCreateNewSheet.statusText,
        data: dataCreateNewSheet.data
    })
}

/** get flexible data : Done test */
export async function GetFlexibleData(req: Request, res: Response){
    const region = req.params.region;
    const from = req.params.from || null;
    const to = req.params.to || null;
    const id = req.body.id || null;
    let range = '';
    if(from == null && to == null){
        range = `${region}!${EnumTextValue.REGION_GET_DATA_DEFAULT}`
    }else{
        range = `${region}!A${from}:Z${to}`
    }
    let data = await GetAllData(range);
    const header = await GetSchema(region) as string[];
    if(data.data.values == null || data.data.values == undefined){
        return res.status(HttpStatusCode.NO_CONENT).json({
            status: HttpStatusCode.NO_CONENT,
            statusText: EnumTextValue.NOT_FOUND,
            data: EnumTextValue.STRING_EMPTY
        })
    }
    let dataConvert = AutoMapingToObject(data.data.values, header);
    
    if(id == null ){
        return res.status(data.status).json({
            status: data.status,
            statusText: data.statusText,
            data: dataConvert
        })
    }
    else{
        let dataFilterById = FindDataByID(dataConvert, id);
        return res.status(data.status).json({
            status: data.status,
            statusText: data.statusText,
            data: dataFilterById
        })
    }
}

/** update flexible data : Done test */
export async function UpdateFlexibleData(req: Request, res: Response){
    const table = req.params.table;
    const id = req.params.id;
    const body = req.body;
    if(id == null){
        return res.status(400).json({
            state: "fail",
            message: 'id does not exist',
            data: null
        })
    }
    const dataConvertToGoogleSheet = CreateSchemaForSheet(body, Object.keys(body).length);
    const range = `${table}!${EnumTextValue.REGION_GET_DATA_DEFAULT}`
    let data = await GetAllData(range);
    if(data.data.values == null || data.data.values == undefined){
        return res.status(HttpStatusCode.NO_CONENT).json({
            status: HttpStatusCode.NO_CONENT,
            statusText: EnumTextValue.NOT_FOUND,
            data: EnumTextValue.STRING_EMPTY
        })
    }
    const arrKeys = Object.keys(body);
    let dataConvert = AutoMapingToObject(data.data.values, arrKeys);
    let dataFileById = FindDataByID(dataConvert, id);
    if(dataFileById.row == null){
        return res.status(HttpStatusCode.NO_CONENT).json({
            status: HttpStatusCode.NO_CONENT,
            statusText: `${range}${EnumTextValue.NOT_EXIST}`,
            data: EnumTextValue.STRING_EMPTY
        })
    }
    let resUpdateData = await UpdateData(dataConvertToGoogleSheet, table, dataFileById.row)
    
    return res.status(resUpdateData.status).json({
        status: resUpdateData.status,
        statusText: resUpdateData.statusText,
        data: resUpdateData.data
    })
}
