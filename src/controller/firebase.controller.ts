import { Request, Response } from "express";
import { UploadImageToStorage } from "../service/firebase.service";
import { EnumTextValue, HttpStatusCode } from "../utils/enum";

/** Function upload image to firebase storage */
export async function UploadImage(req: Request, res: Response) {
    let file = req.file;
    if (file) {
        UploadImageToStorage(file)
        .then((url) => {
            return res.status(HttpStatusCode.OK).json({
                status: HttpStatusCode.OK,
                statusText: EnumTextValue.UPLOAD_SUCCESS,
                data: url as string
            })
        })
        .catch((error) => {
            return res.status(HttpStatusCode.INTERNAL_SERVER_ERROR).json({
                status: HttpStatusCode.INTERNAL_SERVER_ERROR,
                statusText: EnumTextValue.UPLOAD_FAIL,
                data: error
            })
        });
    } else {
        return res.status(HttpStatusCode.BAD_REQUEST).json({
            status: HttpStatusCode.BAD_REQUEST,
            statusText: EnumTextValue.FILE_IS_REQUIRE,
            data: EnumTextValue.STRING_EMPTY
        })
    }
}
