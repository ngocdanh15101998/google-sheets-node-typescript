import { Express } from "express";
import {
    CreateFlexibleData,
    GetFlexibleData,
    UpdateFlexibleData,
    CreateFlexibleSheet
  } from '../controller/googleSheet.controller';
/** tạo router */
//const ProductRouter = express.Router();

function ProductRouter(app: Express){

    /** Get Flexible Data */
    app.post('/api/getflexibledata/:region/:from?/:to?', GetFlexibleData);

    /** Create Flexible Data */
    app.post('/api/createflexibledata', CreateFlexibleData);

    // create new sheet flexible
    app.post('/api/createflexiblesheet', CreateFlexibleSheet);

    /** update flexible data */
    app.put('/api/updateflexibledata/:table/:id', UpdateFlexibleData);

  }


export default ProductRouter;
