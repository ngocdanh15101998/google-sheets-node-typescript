import { Storage } from "@google-cloud/storage";

/** firebase init */
const storage = new Storage({
  projectId: "allxone-shop-fruit",
  keyFilename: './src/service/allxone-shop-fruit-firebase-adminsdk-5tkqk-246ac3f0e0.json',
});
const bucket = storage.bucket("allxone-shop-fruit.appspot.com");

/** Upload image to storage function */
export function UploadImageToStorage(file:Express.Multer.File) {
    return new Promise((resolve, reject) => {
        let newFileName = `${Date.now()}_${file.originalname}`;
        let fileUpload = bucket.file(newFileName);
        
        const blobStream = fileUpload.createWriteStream({
          metadata: {
            contentType: file.mimetype,
          },
        });

        blobStream.on("error", (error) => {
          reject(error);
        });
        blobStream.on("finish", () => {
          const link = `https://firebasestorage.googleapis.com/v0/b/${bucket.name}/o/${fileUpload.name}?alt=media`
          resolve(link);
        });
        blobStream.end(file.buffer);
      });
}