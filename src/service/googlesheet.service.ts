import { google } from 'googleapis';
import dotenv from "dotenv";
dotenv.config();
import config from "config";
import { EnumTextValue } from '../utils/enum';
const spreadsheetId = config.get<string>("spreadSheetId");
const scopes = config.get<string>("scopes");

/** Check authorize google sheet */
export async function CheckAuth() {
    const auth = new google.auth.GoogleAuth({
        keyFile: "allxone-portall-6026d9a4dfee.json",
        scopes: scopes,
      });
    return auth;
}

/** Get info spreadsheet */
export async function GetGoogleSheets(auth:any) {
    const client = await auth.getClient();
    const googleSheets = google.sheets({ version: "v4", auth: client });
    return googleSheets;
}

/** Get All data on sheet */
export async function GetAllData(range:any) {
    const auth = await CheckAuth();
    const googleSheets = await GetGoogleSheets(auth);
    const getData = await googleSheets.spreadsheets.values.get({
    auth,
    spreadsheetId,
    range: range,
    });
    return getData
}

/** Find data by ID */
export function FindDataByID(data:any[], id:string){
  for(let i = 0; i < data.length; i ++){
    if(data[i].id == id){
      return {
        data: data[i],
        row: i+2
      }
    }
  }
  return {
    data: null,
    row: null
  }
  // const result = data.filter((product) => product.id === productId);
}

/** initialization sheet request for create new sheet */
export function SheetRequest(region:any){
    return {
      spreadsheetId: spreadsheetId,
      resource: {
        requests: [
          {
            addSheet: {
              properties: {
                title: region,
              },
            },
          },
        ],
      },
    };
}

/** initialization data request for create first data of sheet */
export function DataRequest(body:any, range:string){
    return {
      spreadsheetId: spreadsheetId,
      range: range, // Điền dữ liệu vào phạm vi A1:B2 của trang tính mới
      valueInputOption: 'USER_ENTERED',
      resource: {
        values: [
          body,
        ],
      },
    };
}

/** Create new sheet into spreadsheet */
export async function CreateNewSheet(region:string, arrKeys:string[] ){
    const auth = await CheckAuth();
    const googleSheets = await GetGoogleSheets(auth);
    const range = `${region}!${EnumTextValue.REGION_CREATE_SHEET_DEFAULT}`;
    const sheetRequest = SheetRequest(region);
      
      // Nhập dữ liệu vào trang tính mới
      const dataRequest = DataRequest(arrKeys, range)
      try {
        const sheetResponse = await googleSheets.spreadsheets.batchUpdate(sheetRequest);
        
        const dataResponse = await googleSheets.spreadsheets.values.update(dataRequest);
        return {
          status: sheetResponse.status,
          statusText: sheetResponse.statusText,
          data: sheetResponse.data
        };
      } catch (err) {
        const error = err as any;
        let errMessage:string = EnumTextValue.STRING_EMPTY;
        for(let i = 0; i < error.errors.length; i++){
          errMessage += error.errors[i].message as string;
        }
        
        return {
          status: error.code as number,
          statusText: errMessage,
          data: null
        };
      }
}

/** Create new data into sheet */
export async function CreateNewData(data:any, region:string){
    const auth = await CheckAuth();
    const googleSheets = await GetGoogleSheets(auth);
  
    // Get the last row of the sheet.
    const lastRow = await googleSheets.spreadsheets.values.get({
      spreadsheetId: spreadsheetId,
      range: `${region}!${EnumTextValue.REGION_GET_ONLY_CELL_A_DEFAULT}`,
      majorDimension: 'ROWS',
    });
    if(lastRow.data.values == null || lastRow.data.values == undefined){
        return false;
    }
     // Set the range of the sheet where you want to add data.
    const range = `${region}!A${lastRow.data.values.length + 1}`;
  
    const dataRequest = DataRequest(data, range);
  
    try {
      const dataResponse = await googleSheets.spreadsheets.values.update(dataRequest);
      return dataResponse;
    } catch (err) {
      console.error(err);
    }
}

/** Get Shema of sheet */
export async function GetSchema(range:string){
    const auth = await CheckAuth();
    const googleSheets = await GetGoogleSheets(auth);
    const firstRow = await googleSheets.spreadsheets.values.get({
      spreadsheetId: spreadsheetId,
      range: `${range}!${EnumTextValue.REGION_GET_FIRST_ROW_DEFAULT}`,
      majorDimension: 'ROWS',
    });
    if(firstRow.data.values == null || firstRow.data.values == undefined){
        return false
    }
    return firstRow.data.values[0];
}

/** Auto mapping data shema from object data schema to google sheet data schema */
export function AutoMapingToGoogleSheet(data:any, schema:any){
    let myArray = new Array(schema.length);
    schema.forEach((value:any, index:number) => {
      if (value in data) {
        myArray[index] = data[value];
      }
    });
    return myArray
}

/** define new schema for google sheet when create new sheet */
export function CreateSchemaForSheet(data:any, count:number){
    let newArray = new Array(count);
    let index = 0;
    for (const key in data) {
      newArray[index] = data[key];
      index++
    }
    return newArray;
}

/** Auto mapping data shema from google sheet data schema to object data schema */
export function AutoMapingToObject(googleSheetData:string[][], header:string[]){
  const res = Array<any>();
  googleSheetData.map((row) => {
    const obj:any = new Object();
    for (let i = 0; i < header.length; i++) {
    obj[header[i]] = EnumTextValue.STRING_EMPTY;
    }

    row.forEach((value, index) => {
      obj[header[index]] = value;
    });
    res.push(obj)
  });
  return res;
}

/** Update data on sheet */
export async function UpdateData(dataConvertToGoogleSheet:any[], table:string, row:number){
  const auth = await CheckAuth();
  const googleSheets = await GetGoogleSheets(auth);
  const valueRange = {
      values: [dataConvertToGoogleSheet],
  };

  var rangUpdate = `${table}!A${row}`;
  const request = {
      spreadsheetId: spreadsheetId,
      range: rangUpdate,
      valueInputOption: 'USER_ENTERED',
      resource: valueRange
  }
  const response = await googleSheets.spreadsheets.values.update(request);
  return response
}