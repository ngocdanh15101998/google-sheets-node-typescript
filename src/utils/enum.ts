export enum EnumTextValue {
    NOT_EXIST = "ID does not exist",
    NOT_FOUND = "Not Found",
    FILE_IS_REQUIRE = 'File is require',
    CREATE_FAIL = "Create fail",
    STRING_EMPTY = "",
    REGION_GET_DATA_DEFAULT = 'A2:ZZZ',
    REGION_CREATE_SHEET_DEFAULT = 'A1:ZZZ',
    REGION_GET_ONLY_CELL_A_DEFAULT = 'A1:A',
    REGION_GET_FIRST_ROW_DEFAULT = '1:1',
    UPLOAD_SUCCESS = 'Upload success',
    UPLOAD_FAIL = 'Upload fail',
}

export enum HttpStatusCode {
    OK = 200,
    CREATED = 201,
    NO_CONENT = 204,
    BAD_REQUEST = 400,
    UNAUTHORIZED = 401,
    FORBIDDEN = 403,
    NOT_FOUND = 404,
    INTERNAL_SERVER_ERROR = 500,
    SERVICE_UNAVAILABLE = 503
  }