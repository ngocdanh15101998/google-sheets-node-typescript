import express, { Request, Response } from "express";
import dotenv from "dotenv";
dotenv.config();
import config from "config";
import responseTime from "response-time";
import connect from "./utils/connect";
import logger from "./utils/logger";
import routes from "./routes";
import deserializeUser from "./middleware/deserializeUser";
import { restResponseTimeHistogram, startMetricsServer } from "./utils/metrics";
import swaggerDocs from "./utils/swagger";
import ProductRouter from "./routes/product.routes";
import cors from 'cors'
const port = config.get<number>("port");

const app = express();

app.use(express.json());

/** Use cors for access */
app.use(cors())

/** midleware check authorize user */

app.use(deserializeUser);

app.use(
  responseTime((req: Request, res: Response, time: number) => {
    if (req?.route?.path) {
      restResponseTimeHistogram.observe(
        {
          method: req.method,
          route: req.route.path,
          status_code: res.statusCode,
        },
        time * 1000
      );
    }
  })
);
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
  res.setHeader('Access-Control-Allow-Credentials', "true");
  next();
});

app.listen(port, async () => {
  // logger.info(`App is running at http://localhost:${port}`);
  /** Function connect DB */
  //await connect();

  /** Router about healthcheck, users, sessions, products, */
  routes(app);
  /** Product router */
  ProductRouter(app);

  startMetricsServer();

  /** enable function doc gen on wagger */
  swaggerDocs(app, port);
});
